#include "l5.h"
int main()
{   //PROBLEMA 2 - inversarea elementelor din coada folosind stiva
    Queue* q;
    q=createQueue();
    for(int i=0; i<5; i++)
        enQueue(q,i+1);
    printf("Coada initiala este: \n");
    display(q);
    printf("\n");
    Node* stackTop=NULL;
    printf("Dupa inversare, coada devine: \n");
    invert(q, &stackTop);


    return 0;
}
