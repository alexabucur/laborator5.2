#include "l5.h"

typedef int Data;
typedef int INT_MIN;


struct Elem
{
    Data val;
    struct Elem* next;
};
typedef struct Elem Node;


Data top(Node *top)
{
    if (isEmptyS(top)) printf("INT_MIN");
    return top->val;
}

void push(Node**top, Data v)
{
    Node* newNode=(Node*)malloc(sizeof(Node));
    newNode->val=v;
    newNode->next=*top;
    *top=newNode;
}

Data pop(Node**top)
{
    if (isEmptyS(*top)) printf("INT_MIN");
    Node *temp=(*top);
    Data aux=temp->val;
    *top=(*top)->next;
    free(temp);
    return aux;
}

int isEmptyS(Node*top)
{
    return top==NULL;
}

void deleteStack(Node**top)
{
    Node  *temp;
    while (!isEmptyS(*top))
        temp=*top;
    *top=(*top)->next;
    free(temp);
}

