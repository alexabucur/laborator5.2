#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef int Data;
typedef struct Elem Node;
typedef struct Q Queue;
//sectiune de predefiniri pentru cozi
Queue* createQueue();
void enQueue(Queue*q, Data v);
Data deQueue(Queue*q);
int isEmptyQ(Queue*q);
void deleteQueue(Queue*q);

//sectune de prdefiniri pentru stive

typedef struct Elem Node;
Data top(Node *top);
void push(Node**top, Data v);
Data pop(Node**top);
int isEmptyS(Node*top);
void deleteStack(Node**top);
